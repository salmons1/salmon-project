import { NavLink } from "react-router-dom";
import "./index.css";
import logo from "./rocket5.png";


export default function Nav(props) {

  return (
    <nav className="navbar">
    {/* <nav className="navbar navbar-expand-lg shadow rounded"> */}
      <div className="d-flex justify-content-start">
      
        <NavLink className="navbar-brand nav-link" to="/"  >  
        <img src={logo} alt="logo" className="logo" />
        projectSpark
        </NavLink>
        <NavLink className=" navbar-brand  nav-link  text-white" aria-current="page" to="/projects"  >Search Projects</NavLink>
        </div>
        <div className="navbar justify-content-end" id="navbarCollapse">
            {!!props.userId ?
              <>
                <li className="nav">
                  <NavLink className="nav-link text-white" to="/profiles/user" state={{ userId: props.userId }}>
                 Profile
                  </NavLink>
                </li>
                <li className="nav me-5">
                  <NavLink className="nav-link text-white"  to="/user/logout">
                   Log Out
                  </NavLink>
                </li>
              </>
              :
              <>
                <li className="nav">
                  <NavLink className="nav-link text-white"  to="/user/login">
                    Log In
                  </NavLink>
                </li>
                <li className="nav me-5">
                  <NavLink className="nav-link text-white" to="/user/signup">
                    Sign Up
                  </NavLink>
                </li>
              </>
            }
        </div>
    </nav>
  )
}