import { Carousel } from "react-bootstrap";
import packed from "./images/packed.png";
import leetcode from "./images/leetcode.png";
// import bootstrap from "./images/bootstrap.png"; 
import nomad from "./images/nomad.png"; 
// import meetme from "./images/meetme.png"; 
import yovies from "./images/yovies.png"; 
// import dog from "./images/dog.png"; 


function CarouselFade() {
  return (
    <Carousel fade className="carousel" interval={1000} >
      <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={leetcode}
          alt="First slide"
        />
      </Carousel.Item>

      <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={packed}
          alt="Second slide"
        />
      </Carousel.Item>

      {/* <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={bootstrap}
          alt="Third slide"
        />
      </Carousel.Item> */}

      <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={nomad}
          alt="Forth slide"
        />
      </Carousel.Item>

      {/* <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={meetme}
          alt="Fifth slide"
        />
      </Carousel.Item> */}

      <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={yovies}
          alt="Sixth slide"
        />
      </Carousel.Item>

      {/* <Carousel.Item >
        <img
          className="d-block w-50"
          style={{ overflow: "hidden" }}
          src={dog}
          alt="Seventh slide"
        />
      </Carousel.Item> */}

    </Carousel>
  );
}

export default CarouselFade;
