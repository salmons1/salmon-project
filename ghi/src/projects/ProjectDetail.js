import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from "react-router-dom"
import { useGetProjectQuery } from '../reduxstore/projectDetailApi'
import { useGetUserQuery } from '../reduxstore/userProfileApi'
import { useCreateCommentMutation, useGetProjectCommentsQuery, useDeleteCommentMutation } from '../reduxstore/projectCommentsApi'
import { Container, Row, Col, Spinner, Table, Form, Card, Button, FloatingLabel } from 'react-bootstrap'
import ReactMarkdown from 'react-markdown'
import {
  PieChart, Pie, Cell, LabelList,
  BarChart, XAxis, YAxis, Bar, ResponsiveContainer,
} from 'recharts'


export default function ProjectDetails(props) {
  // ---STATE VARIABLES---

  // project id from url parameter
  const { id } = useParams();

  // user details from redux store, if user logged in
  const { data: userData } = useGetUserQuery(props.userId, { skip:(!props.userId) })

  // project details from redux store
  const { data: projectData, isLoading: projectLoading } = useGetProjectQuery(id)

  // project comments from redux store
  const { data: commentsData } = useGetProjectCommentsQuery(id)

  // redux mutation for creating new comment
  const [createComment, result] = useCreateCommentMutation()

  // state to store comment text
  const [comment, setComment] = useState("")

  // initialize navigation hook
  const navigate = useNavigate()

  // Settings for datetime conversion in comments
  const dateFormat = { month: 'short', day: 'numeric', year: 'numeric' }
  const timeFormat = { hour: 'numeric', minute: '2-digit' }

  // Settings for Commits Pie Chart
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#F54281', '#B773FF']

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index, data }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.55
    const x = cx + radius * Math.cos(-midAngle * RADIAN)
    const y = cy + radius * Math.sin(-midAngle * RADIAN)

    return (
      <text x={x} y={y} fill="white" fontSize='20px' textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    )
  }

  // ---HELPER FUNCTIONS---

  // handler function for storing comment text into state
  function handleComment(event) {
    setComment(event.target.value)
  }

  // handler function for creating a new comment
  async function handleSubmit(event) {
    event.preventDefault()
    // assemble comment data
    const d = {
      user_id: props.userId,
      project_id: id,
      text: comment,
    }
    console.log(d)
    // post data through redux mutation
    createComment(d)
  }
  // if comment successful, clear comment text box
  useEffect(() => {
    if (result.isSuccess) {
      setComment("")
    }
  }, [result])

  // Hook for deleting a comment
  const [deleteComment] = useDeleteCommentMutation()

  // Handler for deleting a comment
  function handleDeleteComment(commentId) {
    deleteComment(commentId)
  }


  // ---LOADING SCREEN---
  if (projectLoading) {
    return (
      <Container className="text-white my-5 text-white">
        <Row className="text-white d-flex justify-content-center text-center">
          <h3>Loading Gitlab Data...</h3>
          <Spinner className="text-white mt-2" animation="border" />
        </Row>
      </Container>
    )
  }

  // ---PAGE COMPONENT---
  return (
    <>
      <Container>
        {/* Project Name */}
        <Row className='text-white my-4 text-center'>
          <span style={{ fontSize: '48px' }}>{projectData.project.p_name}</span>
        </Row>
        <Row>
          <hr />
        </Row>
        {/* Website Preview */}
        <Row className='text-white mt-2'>
          <Col className='text-white px-1'>
            <div className='text-white shadow'>
              <iframe
                src={projectData.project.live_url ? projectData.project.live_url : '../project_not_found.html'}
                title={projectData.project.p_name}
                height="700px"
                width="100%"
              />
            </div>
          </Col>
        </Row>
        <Row className='text-white mt-3'>
          <Col className='text-white pe-5 text-end'>
            {projectData.project.live_url ?
              <a href={projectData.project.live_url} target="_blank" rel="noreferrer" style={{ fontSize: '20px' }}>Open in new tab</a>
              :
              <br />
            }
          </Col>
        </Row>
        {/* Informational Section */}
        <Row className='text-white mb-4 pt-2'>
          {/* Left-side Panel */}
          <Col lg={3} className='text-white ps-3 pe-5'>
            {/* Project Avatar */}
            <Row>
              <Col>
                <a href={projectData.project.p_gitlab} target="_blank" rel="noreferrer">
                  <img src={projectData.project.p_avatar ? projectData.project.p_avatar : "../avatar_1.jpg"} alt="" width="100%" />
                </a>
              </Col>
            </Row>
            {/* Project Name, ID, and start date */}
            <Row className='text-white mt-3'>
              <Col>
                <a href={projectData.project.p_gitlab} target="_blank" rel="noreferrer" className='text-white black' style={{ fontSize: '28px', fontWeight: 'bold' }}>{projectData.project.p_name}</a>
                <br />
                <a href={projectData.project.p_gitlab} target="_blank" rel="noreferrer" className='text-white small_impact_font' style={{ fontSize: '20px' }}>id: {projectData.project.p_id}</a>
                <br />
                <span className='text-white small_impact_font' style={{ fontSize: '14px' }}>Started on {projectData.project.p_start}</span>
                <hr />
              </Col>
            </Row>
            {/* Group Avatar, Name, and ID */}
            <Row>
              <Col>
                <span style={{ fontSize: '22px', fontWeight: 'bold' }}>Gitlab Group:</span>
              </Col>
            </Row>
            <Row className='text-white mt-1'>
              <Col>
                <Table borderless="true" className='text-white ms-1'>
                  <tbody>
                    <tr>
                      <td style={{ width: '50px', height: '50px', whiteSpace: 'nowrap', padding: '0', margin: '0', verticalAlign: 'middle' }}>
                        <a href={projectData.project.g_gitlab} target="_blank" rel="noreferrer"><img src={projectData.project.g_avatar ? projectData.project.g_avatar : "../avatar_1.jpg"} alt="" style={{ maxWidth: "100%", maxHeight: "100%" }} /></a>
                      </td>
                      <td style={{ paddingLeft: '14px' }}>
                        {projectData.project.g_name === 'N/A' ?
                          (<span className='text-white black' style={{ fontSize: '20px' }}>{projectData.project.g_name}</span>) :
                          (<a href={projectData.project.g_gitlab} target="_blank" rel="noreferrer" className='text-white black' style={{ fontSize: '20px' }}>{projectData.project.g_name}</a>)
                        }
                        <br />
                        <a href={projectData.project.g_gitlab} target="_blank" rel="noreferrer" className='text-white small_impact_font' style={{ fontSize: '16px' }}>{projectData.project.g_id ? "id: " + projectData.project.g_id : ""}</a>
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <hr />
              </Col>
            </Row>
            {/* Team Members */}
            <Row>
              <Col>
                <span style={{ fontSize: '22px', fontWeight: 'bold' }}>Members:</span>
              </Col>
            </Row>
            <Row className='text-white mt-1'>
              <Col>
                <Table borderless="true" className='text-white ms-1' size='sm'>
                  <tbody className='text-white'>
                    {projectData.project.members.map(member => {
                      return (
                        <tr key={member.url}>
                          <td style={{ width: '26px', height: '26px', whiteSpace: 'nowrap', padding: '0', margin: '0', verticalAlign: 'middle' }}>
                            <a href={member.url} target="_blank" rel="noreferrer">
                              <img src={member.avatar_url} alt="" style={{ maxWidth: "100%", maxHeight: "100%" }} />
                            </a>
                          </td>
                          <td style={{ paddingLeft: '8px' }}>
                            <a href={member.url} target="_blank" rel="noreferrer" className='text-white black' style={{ fontSize: '18px' }}>
                              {member.name}
                            </a>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
                <hr />
              </Col>
            </Row>
            {/* Language Data */}
            <Row>
              <span style={{ fontSize: '22px', fontWeight: 'bold' }}>Language Usage:</span>
            </Row>
            <Row className='text-white mt-2'>
              <Col>
                <ResponsiveContainer width={"100%"} height={150}>
                  <BarChart width={250} height={150} data={projectData.project.languages} layout="vertical" margin={{ left: 20, right: 40 }}>
                    <XAxis hide axisLine={false} type="number" />
                    <YAxis dataKey="language" type="category" tickLine={false} axisLine={false} style={{fill:"#ffffff"}} />
                    <Bar dataKey="percentage" fill="#8884d8" label={{ position: 'right', fill:"#ffffff" }} />
                  </BarChart>
                </ResponsiveContainer>
                <hr />
              </Col>
            </Row>
            {/* Wireframes */}
            <Row className='text-white mb-2'>
              <span style={{ fontSize: '22px', fontWeight: 'bold' }}>Wireframes:</span>
            </Row>
            {projectData.project.wireframes[0] ?
              projectData.project.wireframes.map(image_url => {
                return (
                  <Row key={image_url} className='text-white mt-2'>
                    <Col>
                      <a href={image_url} target="_blank" rel="noreferrer"><img src={image_url} className='text-white img-fluid' alt="" /></a>
                    </Col>
                  </Row>
                )
              })
              :
              <Row>
                <Col>
                  <span className='text-white black' style={{ fontSize: '20px' }}>N/A</span>
                </Col>
              </Row>
            }
          </Col>
          {/* Main Panel */}
          <Col lg={9} className='text-white ps-0 pe-3'>
            {/* README */}
            <Row>
              <Col>
                <h1>READ ME:</h1>
                <hr />
                <ReactMarkdown>
                  {projectData.project.readme}
                </ReactMarkdown>
                <hr />
              </Col>
            </Row>
            {/* Commits Data */}
            <Row className='text-white mt-2'>
              <Col>
                <h2>Commits Distribution:</h2>
              </Col>
            </Row>
            <Row className='text-white mt-2'>
              {/* Commits Table */}
              <Col lg={3}>
                <Table className='text-white'>
                  <thead>
                    <tr>
                      <th>
                        Member
                      </th>
                      <th>
                        #
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {projectData.project.counts_by_author.map(author => {
                      return (
                        <tr key={author.name}>
                          <td>
                            {author.name}
                          </td>
                          <td>
                            {author.count}
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        Total Commits:
                      </td>
                      <td>
                        {projectData.project.total_commits_num}
                      </td>
                    </tr>
                  </tfoot>
                </Table>
              </Col>
              {/* Commits Pie Chart */}
              <Col lg={8}>
                <PieChart width={550} height={300}>
                  <Pie
                    data={projectData.project.counts_by_author}
                    cx="50%"
                    cy="50%"
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={120}
                    fill="#8884d8"
                    dataKey="count"
                  >
                    <LabelList dataKey="name" position="outside" offset={20} fontWeight='bold' fontSize='20px' stroke="None" />
                    {projectData.project.counts_by_author && projectData.project.counts_by_author.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                  </Pie>
                </PieChart>
              </Col>
            </Row>
            <Row className='text-white mt-4'><hr /></Row>
            <Row className='mt-2 text-black'>
              {/* Comments */}
              <Col>
                <Card>
                  <Card.Header as="h5">Comments</Card.Header>
                  <Card.Body>
                    {userData ?
                      // Add a Comment - Show only if logged in
                      <>
                        <Form onSubmit={handleSubmit}>
                          <Row>
                            {/* Username and Avatar */}
                            <Col xs='auto'>
                              <Row>
                                <Col>
                                  <img src={`../avatar_${userData.user.avatar_id}.jpg`} alt="" width="80px" height="80px" />
                                </Col>
                              </Row>
                              <Row className='mt-2 text-center'>
                                <Col>
                                  {userData.user.username}
                                </Col>
                              </Row>
                            </Col>
                            {/* Comment Box */}
                            <Form.Group as={Col} controlId="comment">
                              <FloatingLabel className='mb-2' label="Add a comment...">
                                <Form.Control
                                  as="textarea"
                                  placeholder="Add a comment..."
                                  rows={4}
                                  style={{ height: "100%" }}
                                  onChange={handleComment}
                                  value={comment}
                                />
                              </FloatingLabel>
                            </Form.Group>
                          </Row>
                          <Row>
                            {/* Submit Button */}
                            <Col></Col>
                            <Col xs='auto'>
                              <Button variant="secondary" type="submit">Comment</Button>
                            </Col>
                          </Row>
                        </Form>
                      </>
                      // If not logged in, prompt to login
                      :
                      <Row>
                        <Col>
                          <span onClick={() => { navigate('/user/login') }}>Login to add a comment.</span>
                        </Col>
                      </Row>
                    }
                    <hr />
                    {/* Comments List */}
                    {!!commentsData ?
                      commentsData.map(comment => {
                        const timestamp = new Date(comment.timestamp)
                        const isAuthor = userData && userData.user && (userData.user.id === comment.user.id);

                        return (
                          <Row key={comment.id} className='mb-4'>
                            <Col xs='auto'>
                              <Row>
                                {/* User Avatar */}
                                <Col>
                                  <img src={`../avatar_${comment.user.avatar_id}.jpg`} alt="" width="80px" height="80px" />
                                </Col>
                              </Row>
                              <Row className='mt-2 text-center'>
                                {/* Username */}
                                <Col>
                                  {comment.user.username}
                                </Col>
                              </Row>
                            </Col>
                            {/* Comment Text */}
                            <Col className='pt-1' style={{ wordBreak: 'break-all' }}>
                              {comment.text}
                            </Col>
                            {/* Timestamp */}
                            <Col xs='auto' className='pe-4 text-end'>
                              {timestamp.toLocaleDateString("en-US", dateFormat)}
                              <br />
                              {timestamp.toLocaleTimeString("en-US", timeFormat)}
                              {/* Delete button: show only if user is comment author (or any other condition you want) */}
                                {isAuthor && (
                                  <div className='mt-2'>
                                    <Button 
                                      variant="light" 
                                      size="sm"
                                      onClick={() => handleDeleteComment(comment.id)}
                                    >
                                      Delete
                                    </Button>
                                  </div>
                                )}
                            </Col>
                          </Row>
                        )
                      })
                      :
                      <Row>
                        <Col>
                          There are no comments yet...
                        </Col>
                      </Row>
                    }
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}