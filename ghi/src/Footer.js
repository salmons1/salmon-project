import React from "react";
import './index.css'

export default function Footer() {
  return (
      <footer className="mt-auto">
        <h2 className="footer nav justify-content-center py-3"> Meet the Team </h2>
        <div>
          <ul className="footer nav justify-content-center py-1">
            <li className="nav-item">
              <a className="nav-link" href="https://gitlab.com/brendanbird12">Brendan</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.linkedin.com/in/yehsun-kang-aa7040164/">Yehsun</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.linkedin.com/in/liying-liao/">Liying</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.linkedin.com/in/teresamtan/">Teresa</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://gitlab.com/chickenjon">Jonathan</a>
            </li>
          </ul>
        </div>
          <div className="footer footer-copyright text-center py-4">
          ©SJP May 2022 | projectSpark
          <a className="footer-link-color p-2" href="https://gitlab.com/salmons1/salmon-project">
            <i className="fa-brands fa-square-gitlab"></i>
          </a>
        </div>
      </footer>
  );
}