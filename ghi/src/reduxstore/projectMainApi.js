import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const projectMainApi = createApi({
  reducerPath: 'main',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_PROJECTS_SERVICE,
    tagTypes: ['ProjectList'],
  }),
  endpoints: builder => ({
    getProjectMain: builder.query({
      query: () => ({ url: '/api/projects/main' }),
      providesTags: ['ProjectList'],
      keepUnusedDataFor: 3600,
    })
  })
})

export const { useGetProjectMainQuery } = projectMainApi