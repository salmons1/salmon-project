import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import Cookies from 'js-cookie'

export const projectDetailApi = createApi({
  reducerPath: 'project',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_PROJECTS_SERVICE,
    // prepareHeaders: (headers) => {
    //   const token = Cookies.get("jwt_access_token")
    //   if (token) {
    //     headers.set('authorization', `Bearer ${token}`)
    //   }
    // }
  }),
  endpoints: (build) => ({
    getProject: build.query({
      query: (id) => ({ url: `/api/projects/${id}` }),
      keepUnusedDataFor: 3600,
    })
  })
})

export const { useGetProjectQuery } = projectDetailApi