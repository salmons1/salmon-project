import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import Cookies from 'js-cookie'

export const userProfileApi = createApi({
  reducerPath: 'user',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_ACCOUNTS_SERVICE,
    // prepareHeaders: (headers) => {
    //   const token = Cookies.get("jwt_access_token")
    //   if (token) {
    //     headers.set('authorization', `Bearer ${token}`)
    //   }
    // }
  }),
  tagTypes: ['User'],
  endpoints: builder => ({
    getUser: builder.query({
      query: (id) => ({ url: `/api/users/${id}` }),
      providesTags: ['User'],
      keepUnusedDataFor: 3600,
    }),
    editUser: builder.mutation({
      query: (data) => ({
        url: `users/update/${data.id}/`,
        body: data,
        method: 'put',
      }),
      invalidatesTags: ['User'],
    })
  })
})

export const {
  useGetUserQuery,
  useEditUserMutation,
} = userProfileApi
