import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { projectCommentsApi } from './projectCommentsApi';
import { projectDetailApi } from './projectDetailApi';
import { projectListApi } from './projectListApi';
import { userProfileApi } from './userProfileApi';

// Redux Persist
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import { projectMainApi } from './projectMainApi';


const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  [projectListApi.reducerPath]: projectListApi.reducer,
  [projectDetailApi.reducerPath]: projectDetailApi.reducer,
  [projectCommentsApi.reducerPath]: projectCommentsApi.reducer,
  [userProfileApi.reducerPath]: userProfileApi.reducer,
  [projectMainApi.reducerPath]: projectMainApi.reducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({serializableCheck: false})
      .concat(projectListApi.middleware)
      .concat(projectDetailApi.middleware)
      .concat(projectCommentsApi.middleware)
      .concat(userProfileApi.middleware)
      .concat(projectMainApi.middleware)
});

export const persistor = persistStore(store)