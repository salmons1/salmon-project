import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import Cookies from 'js-cookie'

export const projectCommentsApi = createApi({
  reducerPath: 'comments',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_PROJECTS_SERVICE,
    // prepareHeaders: (headers) => {
    //   const token = Cookies.get("jwt_access_token")
    //   if (token) {
    //     headers.set('authorization', `Bearer ${token}`)
    //   }
    // }
  }),
  tagTypes: ['CommentList'],
  endpoints: builder => ({
    getProjectComments: builder.query({
      query: (id) => ({ url: `/api/projects/${id}/comments` }),
      providesTags: ['CommentList'],
      keepUnusedDataFor: 3600,
    }),
    createComment: builder.mutation({
      query: data => ({
        url: '/api/projects/new_comment/',
        body: data,
        method: 'post',
      }),
      invalidatesTags: ['CommentList'],
    }), // 1) Add a deleteComment mutation
    deleteComment: builder.mutation({
      query: (commentId) => ({
        url: `/api/projects/comments/delete/${commentId}/`,
        method: 'DELETE',
      }),
      // 2) Invalidate the CommentList so the UI refreshes automatically
      invalidatesTags: ['CommentList'],
    }),
  })
})

export const {
  useGetProjectCommentsQuery,
  useCreateCommentMutation,
  useDeleteCommentMutation,
} = projectCommentsApi