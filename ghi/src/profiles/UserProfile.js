import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useEditUserMutation, useGetUserQuery } from '../reduxstore/userProfileApi'
import { useLocation } from "react-router-dom"
import { Container, Row, Spinner, Col, Form, FloatingLabel, Modal, Table } from 'react-bootstrap'
import { useEffect, useState } from 'react';
import { useGetUserCommentsQuery } from '../reduxstore/userCommentsApi';

export default function UserProfile() {

  const location = useLocation()
  const { userId } = location.state

  const { data: userData, isLoading: userLoading } = useGetUserQuery(userId)
  const [editUser, userResult] = useEditUserMutation()

  const [bio, setBio] = useState("")
  const [editBio, setEditBio] = useState(false)

  const [modalShow, setModalShow] = useState(false)

  const { data: commentsData } = useGetUserCommentsQuery(userId)

  // Settings for datetime conversion in comments
  const dateFormat = { month: 'short', day: 'numeric', year: 'numeric' }
  const timeFormat = { hour: 'numeric', minute: '2-digit' }

  useEffect(() => {
    if (!userLoading) {
      setBio(userData.user.bio)
    }
  }, [userLoading, userData])

  // toggle edit bio form

  function handleBio(event) {
    setBio(event.target.value)
  }

  async function avatarChange(avatar_id) {
    // assemble user data
    const d = {
      id: userId,
      avatar_id: avatar_id,
    }
    // post data through redux mutation
    editUser(d)
  }

  async function handleBioSubmit(event) {
    event.preventDefault()
    // assemble user data
    const d = {
      id: userId,
      bio: bio,
    }
    console.log(d)
    // post data through redux mutation
    editUser(d)
  }
  // if update successful, close all edit modes
  useEffect(() => {
    if (userResult.isSuccess) {
      setEditBio(false)
      setModalShow(false)
    }
  }, [userResult])

  function AvatarSelect(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Select Avatar
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='text-center'>
          <Table borderless="true">
            <tbody>
              <tr>
                <td>
                  <img src="../avatar_1.jpg" onClick={() => { avatarChange(1) }} alt="" width="150px" height="150px" />
                </td>
                <td>
                  <img src="../avatar_2.jpg" onClick={() => { avatarChange(2) }} alt="" width="150px" height="150px" />
                </td>
                <td>
                  <img src="../avatar_3.jpg" onClick={() => { avatarChange(3) }} alt="" width="150px" height="150px" />
                </td>
              </tr>
              <tr>
                <td>
                  <img src="../avatar_4.jpg" onClick={() => { avatarChange(4) }} alt="" width="150px" height="150px" />
                </td>
                <td>
                  <img src="../avatar_5.jpg" onClick={() => { avatarChange(5) }} alt="" width="150px" height="150px" />
                </td>
                <td>
                  <img src="../avatar_6.jpg" onClick={() => { avatarChange(6) }} alt="" width="150px" height="150px" />
                </td>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  // ---LOADING SCREEN---
  if (userLoading) {
    return (
      <Container className="my-5">
        <Row className="d-flex justify-content-center text-center">
          <h3>Loading User Data...</h3>
          <Spinner className="mt-2" animation="border" />
        </Row>
      </Container>
    )
  }

  // ---PAGE COMPONENT---
  return (
    <Container className="my-5" style={{ width: '60em' }}>
      <Row>
        <Col className='px-1'>
          <Card>
            <Card.Header as="h5" className='text-center'>My Profile</Card.Header>
            <Card.Body>
              <Row>
                <Col xs='auto'>
                  <Row>
                    <Col className='text-center'>
                      <img src={`../avatar_${userData.user.avatar_id}.jpg`} width="80px" height="80px" alt="" />
                    </Col>
                  </Row>
                  <Row className='mt-2 text-center'>
                    <Col>
                      {userData.user.username}
                    </Col>
                  </Row>
                  <Row className='mt-2 text-center'>
                    <Col>
                      <Button variant="primary" onClick={() => setModalShow(true)} >Change Avatar</Button>
                    </Col>
                  </Row>
                </Col>
                <AvatarSelect
                  show={modalShow}
                  onHide={() => setModalShow(false)}
                />
                {editBio ?
                  <Col className='mt-2'>
                    <Form onSubmit={handleBioSubmit}>
                      <Form.Group as={Col} controlId="comment">
                        <FloatingLabel className='mb-2' label="Change your bio...">
                          <Form.Control
                            as="textarea"
                            placeholder="Change your bio..."
                            rows={4}
                            style={{ height: "100%" }}
                            onChange={handleBio}
                            value={bio}
                          />
                        </FloatingLabel>
                      </Form.Group>
                      <Row>
                        <Col></Col>
                        <Col xs='auto' className='pe-1'>
                          <Button onClick={() => { setEditBio(false) }}>Cancel</Button>
                        </Col>
                        <Col xs='auto' className='pe-4'>
                          <Button type="submit">Submit</Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                  :
                  <Col className='mt-2'>
                    <Card.Text className='px-2' style={{ border: 'solid 1px rgba(0,0,0,.125)', height: '100px' }}>
                      {userData.user.bio}
                    </Card.Text>
                    <Row>
                      <Col></Col>
                      <Col xs='auto' className='pe-4'>
                        <Button onClick={() => { setEditBio(true) }} className='mt-2' variant="primary">Change Bio</Button>
                      </Col>
                    </Row>
                  </Col>
                }
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {/* Comments */}
      <Row className='mt-5'>
        <Col>
          <Card>
            <Card.Header as="h5">Your Comments</Card.Header>
            <Card.Body>
              {/* Comments List */}
              {!!commentsData ?
                commentsData.comments.map(comment => {
                  return (
                    <Row key={comment.id} className='mb-4'>
                      <Col xs='auto'>
                        <Row>
                          {/* Project Avatar */}
                          <Col>
                            <img src={comment.p_avatar} alt="" width="80px" height="80px" />
                          </Col>
                        </Row>
                        <Row className='mt-2 text-center'>
                          {/* Project Name */}
                          <Col>
                            {comment.p_name}
                          </Col>
                        </Row>
                      </Col>
                      {/* Comment Text */}
                      <Col className='pt-1' style={{ wordBreak: 'break-all' }}>
                        {comment.text}
                      </Col>
                      {/* Timestamp */}
                      <Col xs='auto' className='pe-4 text-end'>
                        {comment.timestamp.toLocaleDateString("en-US", dateFormat)}
                        <br />
                        {comment.timestamp.toLocaleTimeString("en-US", timeFormat)}
                      </Col>
                    </Row>
                  )
                })
                :
                <Row>
                  <Col>
                    You posted no comments yet...
                  </Col>
                </Row>
              }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}