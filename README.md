# projectSpark

- Teresa Tan
- Jonathan Yoo
- Brendan Bird
- Liying Liao
- Yehsun Kang

A central hub to view, collaborate, and comment on all cohort's projects from the 19-week Hack Reactor Software Engineering Immersive program. Feel free to sign up and poke around on our [site](https://salmons1.gitlab.io/salmon-project/)


## <u>***Intended market***</u>

Students of the May cohort and all future cohorts that could benefit from having a website dedicated to displaying sample projects, as well as all Hack Reactor staff and alumni


## <u>***Functionality***</u>

- Anyone can view all final projects from the 19-week Hack Reactor Software Engineering Immersive program
- Only users who signup and login may leave a comment 
- Search function to find projects based on project name, owners, month and year, etc.
- Project detail page features 
    * preview of their website if deployed
    * README file
    * all GitLab group and project information 
    * miniaturized view of wireframe files
    * bar chart of languages used
    * pie chart breaking down the number of commits per member


## <u>***Design***</u>

### Reference the documentation listed below for an in-depth look at the design.

- [API design](docs/apis.md)
- [Data models](docs/data-model.md)
- [GHI](docs/ghi.md)
- [External API Integrations](docs/integrations.md)
- Tech stack: Django, React, Python, JavaScript, PostgreSQL, Docker, Django REST framework, SimpleJWT auth


## <u>***Getting Started***</u>

### In order to enjoy projectSpark, please follow the steps below:

1. Create a new project directory on your local machine and <mark style="background-color: #808080"><span style="color:orange">CD</span></mark> into it
2. Navigate to [our gitlab](https://gitlab.com/salmons1/salmon-project) and clone our repository
3. Run <mark style="background-color: #808080"><span style="color:orange">git clone <clone_url_here></span></mark> into the new directory
4. Make sure [Docker](https://docs.docker.com/get-docker/) is installed on your machine before proceeding  
5. Run <mark style="background-color: #808080"><span style="color:orange">docker volume create pg-admin-salmon</span></mark>
6. Run <mark style="background-color: #808080"><span style="color:orange">docker volume create postgres-data</span></mark>
7. Run <mark style="background-color: #808080"><span style="color:orange">docker volume create salmon-project</span></mark>
8. Run <mark style="background-color: #808080"><span style="color:orange">docker compose up --build</span></mark>
9. In your browser, navigate to [localhost: 3000](http://localhost:3000) to view our application
10. Sign up then login in order to leave a comment on a project
11. Have fun!🙃







