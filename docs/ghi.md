# Graphical Human Interface

## Navbar

Options on the navbar dependent on whether the user is logged in or not. If logged in, the user will have an extra link that leads to the profile page. If not logged in, there will be links to login or sign up.

![logged in navbar](wireframes/logged_in.png)
![logged out navbar](wireframes/logged_out.png)

## Signup or Login

![signup or login](wireframes/signup_login.png)

## Homepage

Whether or not the user is logged in, they will be able to enter a list project page and a details of each project.
Features a scrolling display of projects. 

![home page](wireframes/homepage.png)


## Project List page

List of projects with a search bar for quick project search by IDs, project name, group name, or member names.
This page shows the project logo, project name with its id, group name with its id, members of the team, and the project start date.
Clicking each project name of its logo leads to the project detail page.

![project list page](wireframes/project-list.png)


## Project Detail page

This page loads a entire project with projectId, project name, group name, group id, team members, language usage, commits distributions,started date. A logged in user can write, update, delete comments. 

![project detail page](wireframes/project-detail.png)
