## Aug 19, 2022
* Setting up the Docker Compose and Docker files with the team.
* Creating pgAdmin account by using  PGADMIN_DEFAULT_EMAIL: salmon@email.com / PGADMIN_DEFAULT_PASSWORD: salmon
## Aug 22, 2022
* Setting up authentication using Django with the team.
* Creating models.py for projects_rest microservice with Teresa and Liying.Teresa wrote and Liying and I navigated the code.
Basically Comment and Rating models are ForeignKey to Project model. Tag and TechStack models are ManyToMany relationship to Project model.
## Aug 23, 2022
* We deleted comments from service on yaml because it is foreignkeyed to Project.
* We changed name of mocroservice user to account_rest
* We worked on views.py; creating get_group_members function and get_project_events function using data from GitLab library.
* For this project we don’t need to create model class because we will pull the data from Gitlab repository.
## Aug 24, 2022
* Brendan initiated the react Carousel component for the main page.
* Setting up Heroku Continuous Deployment(CD) on GitLab, was able to deploy Project-R-Us as a Heroku app.
## Aug 29, 2022
* Worked on project view function based on what the gitLab library database. I struggled in finding a way how to add members property and group id because it was url format in project library. I figured I need to connect it to group library somehow to pull the data correctly.
## Aug 30, 2022
* We changed pulling data from GitLab by GroupID to ProjectID because some groups did make group for this project. Refactored view function and collected ProjectID from April and May coherts. Based on that we created ProjectId on Admin page and inpu some project ids.
I worked on accounts_rest setup for login/logout/sign-up component.
## Aug 31, 2022
* I worked on authentication and created login.js for login component, aming to populate login form on frontend but struggled to call the useToken to Login function.
## Sep 01, 2022
* Studied how the token and cookie works, understand the high level concept of authentication process.
## Sep 02, 2022
* Continued on authentication. Set up the Login/Signup/Signout for the frontend using react function components.
Major blocker: test if the user info is saving and token is successfuly sending when log in.
## Sep 06, 2022
* Authentication is now working. Token is produced and is sending when log in. The issue was the data base was using sqlite3 instead of dj_database_url, and didn’t inherit the AbstractBaseUser for the User Abstract model.
## Sep 07, 2022
* Worked on how to toggle down between login/sign-up and logout in the nav bar.
Logically when login only logout button would show on the nav bar but now not working as desired. Have to work on codes on Nav.js to log out function to be working.
## Sep 09, 2022
* Worked on redirecting the login/logout/sign-up page to main mage.
I wrote the loggedIn/loggedOut variables to Nav.js to map out by name of path and
toggle down two variable by giving the boolean value to them.
## Sep 12, 2022
* Continued to worked on the logout function be able to clear the token once hit the logout button. It hasn’t been working because the env. path wasn’t the right one.
I edited the code for the getTokenInternal function on Auth.js to be able to fetch the
token by matching the const url = `${process.env.REACT_APP_ACCOUNTS_SERVICE}/api/me/token/` with views and yaml file.
## Sep 13, 2022
* Worked on the Mainpage design with css & bootstrap.
Figured out how to load imaged to Carousal component.
Tried popular project card underneath the carousal. That would be the strech goal for next week.
## Sep 14, 2022
* Worked on footer component for the Mainpage design with css & bootstrap.
## Sep 15, 2022
* Worked on ghi.md/integrations.md updating for the README.
