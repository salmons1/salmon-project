from django.contrib import admin
from projects_rest.models import ProjectId, UserVO, Comment


@admin.register(ProjectId)
class ProjectIdAdmin(admin.ModelAdmin):
    pass


@admin.register(UserVO)
class UserVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass
