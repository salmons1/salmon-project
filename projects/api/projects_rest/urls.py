from django.urls import path
from projects_rest.views import (
    get_list_of_project_ids,
    get_projects,
    project_detail,
    get_userVO,
    create_comment,
    get_all_comments,
    get_project_comments,
    projects_main,
    delete_comment,
)

urlpatterns = [
    path(
        "projects/ids/",
        get_list_of_project_ids,
        name="get_list_of_project_ids"
    ),
    path("projects/", get_projects, name="get_projects"),
    path(
        "projects/<int:pk>/",
        project_detail,
        name="project_detail"
    ),
    path("projects/new_comment/", create_comment, name="create_comment"),
    path("users/<int:pk>/", get_userVO, name="get_userVO"),
    path(
        "projects/<int:pk>/comments/",
        get_project_comments,
        name="get_project_comments",
    ),
    path("projects/comments/", get_all_comments, name="get_all_comments"),
    path("projects/main/", projects_main, name="projects_main"),
    path("projects/comments/delete/<int:pk>/", delete_comment, name="delete_comment"),
]
