from django.apps import AppConfig


class ProjectsRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "projects_rest"
