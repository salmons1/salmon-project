from django.db import models


class UserVO(models.Model):
    username = models.CharField(max_length=50)
    bio = models.TextField(max_length=500, null=True, blank=True)
    avatar_id = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return f"{self.username}"


class ProjectId(models.Model):
    project_id = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ("-project_id",)

    def __str__(self):
        return f"{self.project_id}"


class Comment(models.Model):
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        UserVO,
        related_name="comments",
        on_delete=models.CASCADE
    )
    project = models.ForeignKey(
        ProjectId, related_name="comments", on_delete=models.CASCADE
    )

    class Meta:
        ordering = ("-timestamp",)

    def __str__(self):
        return f"id:{self.id} - proj:{self.project} - author:{self.user}"
