from django.test import TestCase
from .views import (
    get_project_from_id,
    get_group_from_project,
    get_members_from_project,
)
from django.test import Client


# Create your tests here.
class TestProjectId(TestCase):
    def test_38610368(self):
        # Arrange
        input = 38610368
        # Act
        result = get_project_from_id(input).id
        # Assert
        self.assertEqual(result, 38610368)


class TestGroupId(TestCase):
    def test_56723031(self):
        project = get_project_from_id(38610368)
        # Arrange
        input = project
        # Act
        result = get_group_from_project(input).id
        # Assert
        self.assertEqual(result, 56723031)


class TestMembersList(TestCase):
    def test_list(self):
        project = get_project_from_id(38610368)
        # Arrange
        input = project
        # Act
        result = get_members_from_project(input)
        # Assert
        self.assertIsNotNone(result)


class TestCommentsList(TestCase):
    def test_comments_list(self):
        # Arrange
        c = Client()
        # Act
        response = c.get("projects/comments/")
        result = response.content
        # Assert
        self.assertIsNotNone(result)
