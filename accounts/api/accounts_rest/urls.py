from django.urls import path

from .views import (
    api_user_token,
    get_comments_by_user,
    users,
    get_user,
    get_users,
    create_user,
    delete_user,
    update_user
)

urlpatterns = [
    path("users/", users, name="user_signup"),
    path("tokens/mine/", api_user_token, name="user_token"),
    path("users/", get_users, name="get_users"),
    path("users/<int:pk>/", get_user, name="user_detail"),
    path("users/new/", create_user, name="create_user"),
    path("users/delete/<int:pk>/", delete_user, name="delete_user"),
    path("users/update/<int:pk>/", update_user, name="update_user"),
    path(
        "users/comments/<int:pk>",
        get_comments_by_user,
        name="user_comments",
    ),
]
